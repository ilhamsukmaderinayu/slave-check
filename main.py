import requests
import json
import os
from datetime import datetime, timedelta
import smtplib
import time
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders

config_file = "/usr/local/slave-check/config.json"
data_cfg = json.load(open(config_file))
url = data_cfg["url"]
list_slaves = data_cfg["ip_slaves"]

def get_active_slaves(url):
	slave_active=[]
	r = requests.get(str(url))
	data_slaves=r.json()
	for x in range(len(data_slaves)):
		slave_active.append(data_slaves['slaves'][x]['hostname'])
	return slave_active

def slaves_inactive(url,list_slave):
	active=get_active_slaves(url)
	dead=list(set(list_slave) - set(active))
	return dead

def checker():
	dead=slaves_inactive(url,list_slaves)
	active=get_active_slaves(url)
	slave=[]
	if len(dead)==0:
		for i in active:
			temp = dict()
			ping_check = os.system("ping -c 1 " + i)
			temp["ip_slave"]=i
			temp["status_slave"]="slave connect"
			if ping_check == 0:
				temp["status_ping"]="ping ok"
			else:
				temp["status_ping"]="cant ping"
			slave.append(temp)
	else:
		for x in list_slaves:
			for i in dead:
				if x==i:
					ping_check = os.system("ping -c 1 " + i)
					temp=dict()
					temp["ip_slave"]=i
					temp["status_slave"]="slave disconnect"
					if ping_check == 0:
						temp["status_ping"]="ping ok"
					else:
						temp["status_ping"]="cant ping"
					slave.append(temp)
				else: 
					ping_check = os.system("ping -c 1 " + x)
					temp=dict()
					temp["ip_slave"]=x
					temp["status_slave"]="slave connect"
					temp["status_ping"]="cant ping"
					if ping_check == 0:
						temp["status_ping"]="ping ok"
					else:
						temp["status_ping"]="cant ping"
					slave.append(temp)
	return slave

def create_body(date_check,hour_report):
	body = "Berikut kami lampirkan hasil monitoring Server Slave Polda Jawa Timur: "+"\n"+"Tanggal : " + str(date_check)+"\n"+"Jam : "+str(hour_report)+".00 WIB"+"\n"
	slave_checker=checker()
	count = 0
	slave_status=""
	for i in slave_checker:
		count=count+1
		slave_status=slave_status+str(count)+". IP Slave: "+i["ip_slave"]+", Status Slave: "+i["status_slave"]+", Status ping: "+i["status_ping"]+"\n"
	body=body+slave_status
	
	return body

def send_email(username,password,recipient,message,date_report,hour_report):

    attachment = []
    msg = MIMEMultipart() 
    msg['From'] = username 
    msg['To'] = recipient
    msg['Subject'] = "Monitoring Server Slave Polda Jawa Timur Report as "+str(date_report)+":"+str(hour_report)+".00 WIB"
    body = message
    msg.attach(MIMEText(body, 'plain'))
    text = msg.as_string()

    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login(username, password)
        server.sendmail(username, recipient, text)
        server.close()
        print("successfully sent the mail")
    except:
        print("failed to send mail")

def do_checking():

	current_time = datetime.now()
	date_check  = str(current_time.year)+'-'+str(current_time.month)+'-'+str(current_time.day)
	message=create_body(date_check,current_time.hour)
	send_email("rizkifika@nodeflux.io","rizkifika121","support@nodeflux.io",message,date_check,current_time.hour)
if __name__ == '__main__':
	do_checking()